import time
from _datetime import datetime
from send_json import *
from config_file import *
def message_controller(rcvd_json):
    sender = rcvd_json[0]
    recipient = rcvd_json[1]
    timestamp = rcvd_json[2]
    message = rcvd_json[3]
    print(timestamp)
    print(message)
    if len(rcvd_json) == 5 :
        quick_reply = 'T'
        if message == 'connectIPS':
            send_typing(sender)
            time.sleep(2)
            send_buttons_carousel_cips(sender)
        elif message == 'NCHL-ECC':
            send_typing(sender)
            time.sleep(2)
            send_buttons_carousel_ecc(sender)
        elif message == 'NCHL-IPS':
            send_typing(sender)
            time.sleep(2)
            send_buttons_carousel_ips(sender)
        elif message == 'Stop Bot':
            send_typing(sender)
            time.sleep(2)
            send_quick_replies_2(sender)
        elif message == '24 Hrs':
            send_typing(sender)
            time.sleep(2)
            bot.send_text_message(sender,'Bot has been stopped for 24 Hrs. Please leave your message and we will get back to you soon!')

        elif message == '1 Week':
            send_typing(sender)
            time.sleep(2)
            bot.send_text_message(sender,
                                  'Bot has been stopped for 1 Week. Please leave your message and we will get back to you soon!')
        elif message == '1 Month':
            send_typing(sender)
            time.sleep(2)
            bot.send_text_message(sender,
                                  'Bot has been stopped for 1 Month. Please leave your message and we will get back to you soon!')
        elif message == 'Survey':
            send_typing(sender)
            time.sleep(2)
            send_quick_replies_survey(sender)

        elif message == 'Contact Us':
            send_typing(sender)
            time.sleep(2)
            send_quick_reply_contact(sender)

        elif message == 'Report a problem':
            send_typing(sender)
            time.sleep(2)
            send_quick_reply_report(sender)

        elif message == 'Friends' or 'Facebook' or 'News':
            send_typing(sender)
            time.sleep(2)
            bot.send_text_message(sender,'Thank you for your cooperation!')

    else:
        print('not quick reply')
        file = open(filename + str(sender) + '.log', 'r')
        last_line = file.read()
        file.close()
        if (last_line):
            last_timestamp = (last_line.split(":"))[1]
            duration = timestamp - int(last_timestamp)
            print(duration)
            if duration>1000:
                file = open(filename + str(sender) + '.log', 'w')
                file.write(str(sender) + ":" + str(timestamp) + ":" + str(message))
                file.close()
                send_typing(sender)
                time.sleep(2)
                send_quick_replies_1(sender)

